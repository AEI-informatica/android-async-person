package com.appsfromholland.track_4_async_person;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements RandomUserTask.OnRandomUserAvailable,
        View.OnClickListener {

    // TAG for Log.i(...)
    private static final String TAG = "MainActivity";

    private Button addOnePersonBtn = null;
    private ListView personsListView = null;

    //
    private ArrayList<Person> persons = new ArrayList<Person>();
    private PersonAdapter personAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inflate UI and set listeners and adapters and ...
        personsListView = (ListView) findViewById(R.id.personslistView);
        personAdapter = new PersonAdapter(getApplicationContext(),
                getLayoutInflater(),
                persons);
        personsListView.setAdapter(personAdapter);

        addOnePersonBtn = (Button) findViewById(R.id.addPersonButton);
        addOnePersonBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.i(TAG, "onClick aangeroepen");

        String[] urls = new String[] { "https://randomuser.me/api/" };

        // Connect and pass self for callback
        RandomUserTask getRandomUser = new RandomUserTask(this);
        getRandomUser.execute(urls);
    }

    /**
     * Dit is de callback methode die door de RandomUserTask wordt aangeroepen
     * zodra er een random user opgehaald is.
     *
     * @param person
     */
    @Override
    public void onRandomUserAvailable(Person person) {
        // Toevoegen array
        persons.add(person);
        Log.i(TAG, "Person added (" + person.toString() + ")");
        personAdapter.notifyDataSetChanged();
    }
}
